import datasets
import numpy as np
import pandas as pd
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from train import HF_SAS, MODEL_NAME
from sklearn.pipeline import Pipeline
from transformers import T5TokenizerFast
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.preprocessing import Normalizer, StandardScaler
from sklearn.metrics import accuracy_score

# read in data
dataset = datasets.load_from_disk(HF_SAS)
sources, targets = zip(*[(i['source'], i['target']) for i in dataset['train']['translation']])


# feature engineering
t5tokenizer = T5TokenizerFast.from_pretrained(MODEL_NAME)


def tokenize(text_list):
    token_ids = t5tokenizer(text_list,
                            max_length='max',
                            padding='do_not_pad',
                            truncation=False)['input_ids']
    # token_ids = [i for i in token_id if i != 1 for token_id in token_ids]

    return token_ids


vectorizer = CountVectorizer(lowercase=False, tokenizer=tokenize)
X = vectorizer.fit_transform(list(sources) + list(targets)).toarray()
y = len(sources) * [0, ] + len(targets) * [1, ]

# pipeline_svm = Pipeline([("normalizer", Normalizer(norm="l1")),
#                      ("scaler", StandardScaler()),
#                      ("Linear SVM", SVC(max_iter=-1, probability=True, break_ties=True,
#                                        random_state=42))])
#
# param_grid = {'kernel': ['linear', 'poly', 'rbf'],
#               'C': np.power(10, np.arange(-5, 5, dtype=float)),
#               'degree': np.linspace(1, 5, 5, dtype=int),
#               'gamma': ['scale', 'auto', 1, 0.1, 0.01, 0.001, 0.0001],
#               'coef0': [0, 10, 100, 1000] }
#
# search_results = []
# for grid in list(ParameterGrid(param_grid)):
#     search_result = {}
#     pipeline_svm[2].set_params(**grid)
#     pipeline_svm.fit(X_train_ebg_wps, y_train_ebg)
# #     print(f'Accuray on test set is {pipeline_svm.score(X_test_ebg_wps, y_test_ebg)}')
#     search_result['param'] = grid
#     search_result['accuracy'] = pipeline_svm.score(X_test_ebg_wps, y_test_ebg)
#     search_results.append(search_result)
#
# pickle.dump(search_results, open('grid_search_ebg.pkl', 'wb'))


pipeline_svm = Pipeline([("normalizer", Normalizer(norm="l1")),
                         ("scaler", StandardScaler()),
                         ("svm", SVC(kernel='linear', C=1.0, max_iter=-1, random_state=42))])


pipeline_svm.fit(X, y)

id_token_weight = {'id': vectorizer.get_feature_names_out(),
                   'token': t5tokenizer.convert_ids_to_tokens(vectorizer.get_feature_names_out()),
                   'weight': pipeline_svm[-1].coef_.tolist()[0]}

df = pd.DataFrame.from_dict(id_token_weight)

max20 = df.sort_values(['weight'], ascending=False).head(20)
min20 = df.sort_values(['weight'], ascending=True).head(20)




pipeline_logistic_regression = Pipeline([
                         ("normalizer", Normalizer(norm="l1")),
                         ("scaler", StandardScaler()),
                         ("logistic_regression", LogisticRegression(C=1.0, random_state=42))])

pipeline_logistic_regression.fit(X, y)

id_token_weight = {'id': vectorizer.get_feature_names_out(),
                   'token': t5tokenizer.convert_ids_to_tokens(vectorizer.get_feature_names_out()),
                   'weight': pipeline_logistic_regression[-1].coef_.tolist()[0]}

df = pd.DataFrame.from_dict(id_token_weight)
max40 = df.sort_values(['weight'], ascending=False).head(40)
min40 = df.sort_values(['weight'], ascending=True).head(40)

accuracy_score(y, pipeline_logistic_regression.predict(X))