# coding=utf-8

"""
This module
1. reads in PNAS Abstract-Significance corpus crawled from the Internet Archive.
2. splits 200 dev and 200 test samples from the corpus.
3. injects character noise into the fixed dev and test splits.
4. makes three copies of training data with injected character noise.
5. saves files into .jsonl format as per to convention in Datasets library.

"""

__author__ = "hw56@indiana.edu"
__version__ = "1.0.0"
__license__ = "ISC"

import re
import json
import datasets
from utilities import list2jsonl
from sklearn.model_selection import train_test_split


# path to PNAS corpus
pnas_corpus_dir = 'pnas_corpus/pnas.jl'
# save dirs
train_file_path = 'resource/sas_corpus/sas_train.jsonl'
val_file_path = 'resource/sas_corpus/sas_validation.jsonl'
test_file_path = 'resource/sas_corpus/sas_test.jsonl'
SAS_DATASET_PATH = 'resource/sas_dataset_hf'

if __name__ == '__main__':
    print("*" * 100)
    print("Starts building Scientific Abstract Simplification (SAS) Corpus")
    # extract raw corpus from crawled file
    with open(pnas_corpus_dir) as f:
        lines = f.readlines()
    raws = [json.loads(line) for line in lines]
    raws = [raw for raw in raws if (raw['doi'] and raw['significance'] and raw['abstract'])]
    # remove html tags kept in some fields
    to_remove_expr = re.compile('<.*?>')
    for raw in raws:
        for field in ['title', 'significance', 'abstract']:
            raw[field] = re.sub(to_remove_expr, "", raw[field])
    # extract useful fields
    abstract_list = [raw['abstract'] for raw in raws]
    significance_list = [raw['significance'] for raw in raws]
    title_list = [raw['title'] for raw in raws]
    doi_list = [raw['doi'] for raw in raws]
    discipline_list = [raw['discipline'] if raw['discipline'] else 'UNKNOWN' for raw in raws]

    # organize sas corpus into a list of dicts
    sas_corpus = []
    for a, s, t, d, p in zip(*[abstract_list, significance_list, title_list, doi_list, discipline_list]):
        _dict = {'translation': {'source': a, 'target': s, 'doi': d, 'title': t, 'discipline': p}}
        sas_corpus.append(_dict)

    # split data_dict list into train/dev/test
    train_data, val_test_data = train_test_split(sas_corpus, test_size=400, random_state=42)
    val_data, test_data = train_test_split(val_test_data, test_size=200, random_state=42)
    list2jsonl(val_data, val_file_path)
    list2jsonl(test_data, test_file_path)
    list2jsonl(train_data, train_file_path)
    print("Generated data splits are saved.")
    # convert sas corpus into datasets.dataset_dict.DatasetDict
    sas = datasets.load_dataset('json', data_files={'train': train_file_path,
                                                    'validation': val_file_path,
                                                    'test': test_file_path})
    sas.save_to_disk(SAS_DATASET_PATH)
    print("Hugginface DatasetDict SAS has been saved.")
    print("*" * 100)
