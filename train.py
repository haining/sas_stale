# coding=utf-8

__author__ = "hw56@indiana.edu"
__version__ = "alpha"
__license__ = "ISC"

import os
import torch
import datasets
import argparse
import numpy as np
import pandas as pd
from constants import *
from evaluate import load
from itertools import chain
import pytorch_lightning as pl
from torch.utils.data import Dataset, DataLoader
from pytorch_lightning.loggers import WandbLogger
from utilities import prepare_datasets_split_for_concat
from transformers import T5TokenizerFast, T5ForConditionalGeneration
from pytorch_lightning.callbacks import (ModelCheckpoint,
                                         LearningRateMonitor,
                                         EarlyStopping,
                                         TQDMProgressBar)

# environment variable
# os.environ['CUDA_LAUNCH_BLOCKING'] = '1'
os.environ["WANDB_CACHE_DIR"] = "./wandb"
os.environ["HF_DATASETS_CACHE"] = "./huggingface"


class SASDataset(Dataset):
    def __init__(
            self,
            data: list,  # a list of dicts of paired texts, keyed by, e.g., `source` `target` `discipline` `title` `doi`
            task: str,
            tokenizer_name: str = BASE_MODEL_NAME,
            max_source_length: int = MAX_SOURCE_LENGTH,
            max_target_length: int = MAX_TARGET_LENGTH,
    ):
        self.data = data
        self.task = task
        self.tokenizer = T5TokenizerFast.from_pretrained(tokenizer_name)
        self.max_source_length = max_source_length
        self.max_target_length = max_target_length

    def __len__(self):
        return len(self.data)

    def __getitem__(self, index: int):
        datum = self.data[index]
        target_text = datum['target']
        source_text = datum['source']
        task_instruction = TASK2INSTRUCTION[self.task]
        encoding = self.tokenizer(task_instruction + source_text,
                                  max_length=self.max_source_length, padding='max_length', truncation=True,
                                  return_tensors='pt')
        target_encoding = self.tokenizer(target_text,
                                         max_length=self.max_source_length, padding='max_length', truncation=True)
        # replace all tokenizer.pad_token_id in the labels by -100 when we want to ignore padding in the loss
        labels = torch.tensor(
            [(label if label != self.tokenizer.pad_token_id else -100) for label in target_encoding['input_ids']],
            dtype=torch.int64)

        return dict(
            input_ids=encoding['input_ids'].flatten(),
            attention_mask=encoding['attention_mask'].flatten(),
            labels=labels.flatten(),
            target_text=target_text,
            source_text=source_text)


class SASMixtureDataset(Dataset):
    def __init__(
            self,
            data: Dataset,  # a Dataset instance of three keys, i.e., `source` `target` `task`
            data_split: str,
            corpus_names: list,
            task_length_dist: dict = TASK_LENGTH_DIST_TRAIN,
            tokenizer_name: str = BASE_MODEL_NAME,
            max_source_length: int = MAX_SOURCE_LENGTH,
            max_target_length: int = MAX_TARGET_LENGTH):
        self.data = data
        self.data_split = data_split
        self.corpus_names = corpus_names
        self.task_length_dist = task_length_dist
        self.task = None
        self.tokenizer = T5TokenizerFast.from_pretrained(tokenizer_name)
        self.max_source_length = max_source_length
        self.max_target_length = max_target_length

    def __len__(self):
        return len(self.data)

    def __getitem__(self, index: int):
        # we will only ignore `index` during training to enumerate each dataset wrt its challenge
        if self.data_split == 'train':
            total_steps = sum([self.task_length_dist[c] for c in self.corpus_names])
            self.task = np.random.choice(self.corpus_names, 1,
                        p=[self.task_length_dist[c]/total_steps for c in self.corpus_names],
                        replace=True)[0]
            task_len = self.task_length_dist[self.task]
            task_instruction = TASK2INSTRUCTION[self.task]
            if self.task == 'sas_baseline':
                global sas_count
                target_text = self.data[: task_len]['target'][sas_count % task_len]
                source_text = self.data[: task_len]['source'][sas_count % task_len]
                task_identifier = self.data[: task_len]['task'][sas_count % task_len]
                sas_count += 1
                wandb_logger.log_metrics({'train_sample_count/sas_baseline': sas_count})

            elif self.task == 'contextualization':
                if TASK == 'sas_full':
                    index_start = self.task_length_dist['sas_baseline']
                elif TASK == 'sas_pretraining':
                    index_start = 0
                global contextualization_count
                target_text = self.data[index_start: index_start + task_len]['target'][
                    contextualization_count % task_len]
                source_text = self.data[index_start: index_start + task_len]['source'][
                    contextualization_count % task_len]
                task_identifier = self.data[index_start: index_start + task_len]['task'][
                    contextualization_count % task_len]
                contextualization_count += 1
                wandb_logger.log_metrics({'train_sample_count/contextualization': contextualization_count})

            elif self.task == 'summarization':
                if TASK == 'sas_full':
                    index_start = self.task_length_dist['sas_baseline'] + self.task_length_dist['contextualization']
                elif TASK == 'sas_pretraining':
                    index_start =  self.task_length_dist['contextualization']
                global summarization_count
                target_text = self.data[index_start: index_start + task_len]['target'][
                    summarization_count % task_len]
                source_text = self.data[index_start: index_start + task_len]['source'][
                    summarization_count % task_len]
                task_identifier = self.data[index_start: index_start + task_len]['task'][
                    summarization_count % task_len]
                summarization_count += 1
                wandb_logger.log_metrics({'train_sample_count/summarization': summarization_count})

            elif self.task == 'simplification':
                if TASK == 'sas_full':
                    index_start = self.task_length_dist['sas_baseline'] + self.task_length_dist['contextualization'] + \
                                  self.task_length_dist['summarization']
                elif TASK == 'sas_pretraining':
                    index_start = self.task_length_dist['contextualization'] + self.task_length_dist['summarization']
                global simplification_count
                target_text = self.data[index_start: index_start + task_len]['target'][simplification_count % task_len]
                source_text = self.data[index_start: index_start + task_len]['source'][simplification_count % task_len]
                task_identifier = self.data[index_start: index_start + task_len]['task'][
                    simplification_count % task_len]
                simplification_count += 1
                wandb_logger.log_metrics({'train_sample_count/simplification': simplification_count})
            else:
                raise RuntimeError(f"Unknown task {self.task}.")
            assert task_identifier == self.task, RuntimeError(
                f'task {self.task} does not match datum {task_identifier} at position {str(sas_count % task_len)}.')
        else:
            datum = self.data[index]
            self.task = datum['task']
            target_text = datum['target']
            source_text = datum['source']
            task_instruction = TASK2INSTRUCTION[self.task]

        encoding = self.tokenizer(task_instruction + source_text,
                                  max_length=self.max_source_length, padding='max_length', truncation=True,
                                  return_tensors='pt')
        target_encoding = self.tokenizer(target_text,
                                         max_length=self.max_source_length, padding='max_length', truncation=True)
        # replace all tokenizer.pad_token_id in the labels by -100 when we want to ignore padding in the loss
        labels = torch.tensor(
            [(label if label != self.tokenizer.pad_token_id else -100) for label in target_encoding['input_ids']],
            dtype=torch.int64)
        return dict(
            input_ids=encoding['input_ids'].flatten(),
            attention_mask=encoding['attention_mask'].flatten(),
            labels=labels.flatten(),
            target_text=target_text,
            source_text=source_text,
            task=self.task)


class SASDataModule(pl.LightningDataModule):
    """
    It loads huggingface format dataset ('datasets.dataset_dict.DatasetDict') from disk and return train/val/test
    dataloaders.
    Used for both SAS and XSum tasks.
    """

    def __init__(
            self,
            task: str,
            tokenizer: str = BASE_MODEL_NAME,
            batch_size: int = BATCH_SIZE,
            max_source_length: int = MAX_SOURCE_LENGTH,
            max_target_length: int = MAX_TARGET_LENGTH,
            num_workers: int = NUM_WORKERS
    ):
        super().__init__()
        self.batch_size = batch_size
        self.tokenizer = tokenizer
        self.max_source_length = max_source_length
        self.max_target_length = max_target_length
        self.num_workers = num_workers
        self.task = task
        if self.task == 'sas_baseline':
            self.dataset = datasets.load_from_disk(SAS_DATASET_PATH)
        elif self.task == 'simplification':
            self.dataset = datasets.load_from_disk(WIKIAUTO_DATASET_PATH)
        elif self.task == 'summarization':
            self.dataset = datasets.load_from_disk(CNN_DAILYMAIL_DATASET_PATH)
        elif self.task == 'contextualization':
            self.dataset = datasets.load_from_disk(SC_DATASET_ABS2CONT_PATH)
        elif self.task == 'sas_full':  # todo: check sequence
            self.dataset = [(datasets.load_from_disk(SAS_DATASET_PATH), 'sas_baseline'),
                            (datasets.load_from_disk(SC_DATASET_ABS2CONT_PATH), 'contextualization'),
                            (datasets.load_from_disk(CNN_DAILYMAIL_DATASET_PATH), 'summarization'),
                            (datasets.load_from_disk(WIKIAUTO_DATASET_PATH), 'simplification')]
        elif self.task == 'sas_pretraining':  # todo: check sequence
            self.dataset = [(datasets.load_from_disk(SC_DATASET_ABS2CONT_PATH), 'contextualization'),
                            (datasets.load_from_disk(CNN_DAILYMAIL_DATASET_PATH), 'summarization'),
                            (datasets.load_from_disk(WIKIAUTO_DATASET_PATH), 'simplification')]
        elif self.task == 'ablation_no_contextualization':
            pass
        elif self.task == 'ablation_no_simplification':
            pass
        elif self.task == 'alation_no_summarization':
            pass
        self.train_dataset = None
        self.val_dataset = None
        self.test_dataset = None

    def setup(self, stage=None):
        # monotask training
        if self.task in ['sas_baseline', 'contextualization', 'simplification', 'summarization']:
            self.train_dataset = SASDataset(
                self.dataset['train']['translation'],
                self.task, self.tokenizer, self.max_source_length, self.max_target_length)
            self.val_dataset = SASDataset(
                self.dataset['validation']['translation'],
                self.task, self.tokenizer, self.max_source_length, self.max_target_length)
            self.test_dataset = SASDataset(
                self.dataset['test']['translation'],
                self.task, self.tokenizer, self.max_source_length, self.max_target_length)
        # elif self.task == 'sas_full':
        # multitask training
        else:
            train_mixture = datasets.concatenate_datasets([
                prepare_datasets_split_for_concat(corpus, 'train', name) for corpus, name in self.dataset])
            val_mixture = datasets.concatenate_datasets([
                prepare_datasets_split_for_concat(corpus, 'validation', name) if name != 'summarization' else
                prepare_datasets_split_for_concat(corpus, 'validation', name).select(indices=range(VALIDATION_CAP))
                for corpus, name in self.dataset])
            test_mixture = datasets.concatenate_datasets([
                prepare_datasets_split_for_concat(corpus, 'test', name) for corpus, name in self.dataset])
            self.train_dataset = SASMixtureDataset(data=train_mixture, data_split='train',
                                                   corpus_names=[t for _, t in self.dataset],
                                                   task_length_dist=TASK_LENGTH_DIST_TRAIN)
            self.val_dataset = SASMixtureDataset(data=val_mixture, data_split='validation',
                                                 corpus_names=[t for _, t in self.dataset],
                                                 task_length_dist=TASK_LENGTH_DIST_VAL)
            self.test_dataset = SASMixtureDataset(data=test_mixture, data_split='test',
                                                  corpus_names=[t for _, t in self.dataset],
                                                  task_length_dist=TASK_LENGTH_DIST_TEST)

    def train_dataloader(self):
        if self.task in ['sas_baseline', 'contextualization', 'summarization', 'simplification', 'sas_full', 'sas_pretraining']:
            return DataLoader(
                self.train_dataset, batch_size=self.batch_size, shuffle=True, num_workers=self.num_workers)

    def val_dataloader(self):
        if self.task in ['sas_baseline', 'contextualization', 'summarization', 'simplification', 'sas_full', 'sas_pretraining']:
            return DataLoader(
                self.val_dataset, batch_size=self.batch_size, shuffle=False, num_workers=self.num_workers)

    def test_dataloader(self):
        if self.task in ['sas_baseline', 'contextualization', 'summarization', 'simplification', 'sas_full', 'sas_pretraining']:
            return DataLoader(
                self.test_dataset, batch_size=self.batch_size, shuffle=False, num_workers=self.num_workers)


class SASModel(pl.LightningModule):
    """
    An efficient-t5-based translation model built for scientific abstract simplification.
    """

    def __init__(self,
                 model_name: str = BASE_MODEL_NAME,
                 log_text: bool = LOG_TEXT,
                 metrics: list = METRICS,
                 fp16: bool = False):
        super().__init__()
        self.model = T5ForConditionalGeneration.from_pretrained(model_name)
        self.tokenizer = T5TokenizerFast.from_pretrained(model_name)
        self.log_text = log_text
        self.fp16 = fp16
        # metrics
        self.metrics = metrics
        self.metric_sacrebleu = load('sacrebleu')
        self.metric_meteor = load('meteor')
        # self.metric_bertscore = load('bertscore')
        self.metric_sari = load('sari')
        self.metric_rouge = load('rouge')
        self.metric_bleu = load('bleu')
        self.log_text_container = None
        self.save_hyperparameters()

    def forward(self, input_ids, attention_mask, labels):
        output = self.model(input_ids=input_ids, attention_mask=attention_mask, labels=labels)

        return output.loss, output.logits

    def training_step(self, batch, batch_size):
        input_ids = batch['input_ids']
        attention_mask = batch['attention_mask']
        labels = batch['labels']
        decoder_input_ids = self.model._shift_right(labels)

        loss, logits = self(
            input_ids=input_ids,
            attention_mask=attention_mask,
            labels=labels,
            # use_cache=False
        )
        self.log("train_loss", loss, batch_size=batch_size, sync_dist=True)

        return loss

    def on_validation_start(self):
        # init wandb containers
        if self.log_text and self.global_step % LOG_TEXT_INTERVAL == 0:
            self.log_text_container = pd.DataFrame(columns=['source_texts', 'generated_texts', 'target_texts'])

    def validation_step(self, batch, batch_size):
        input_ids = batch['input_ids']
        attention_mask = batch['attention_mask']
        labels = batch['labels']

        loss, logits = self(
            input_ids=input_ids,
            attention_mask=attention_mask,
            labels=labels)
        # the output of self() is: odict_keys(['loss', 'logits', 'past_key_values', 'encoder_last_hidden_state'])
        self.log("val_loss", loss, batch_size=batch_size, sync_dist=True)

        # compute metrics only when
        # if self.log_text and self.global_rank == 0 and self.global_step % LOG_TEXT_INTERVAL == 0:
        if self.log_text and self.global_step % LOG_TEXT_INTERVAL == 0:
            self._generate_step(batch, 'val')

        return loss

    def _generate_step(self, batch, data_split: str):
        assert data_split in ['val', 'test']
        # generate given decoding strategy and compute metrics
        source_texts = batch['source_text']
        target_texts = [[r.strip()] for r in batch['target_text']]  # a list of lists

        # log decoding
        output_sequences = self.model.generate(
            input_ids=batch["input_ids"],
            attention_mask=batch["attention_mask"],
            max_length=MAX_TARGET_LENGTH,
            top_p=TOP_P_PROB,
            repetition_penalty=REPETITION_PENALTY,
            do_sample=True)
        # compute common metrics: sacrebleu and meteor
        predictions = [p.strip() for p in self.tokenizer.batch_decode(output_sequences, skip_special_tokens=True)]
        metrics = self.calculate_common_metrics(source_texts, predictions, target_texts)
        # logging metrics and texts
        for m in self.metrics:
            if m == 'rouge':
                wandb_logger.log_metrics({f"{data_split}_{str(self.global_step)}/{'rouge1'}": metrics['rouge1']})
                wandb_logger.log_metrics({f"{data_split}_{str(self.global_step)}/{'rouge2'}": metrics['rouge2']})
                wandb_logger.log_metrics({f"{data_split}_{str(self.global_step)}/{'rougeL'}": metrics['rougeL']})
                wandb_logger.log_metrics({f"{data_split}_{str(self.global_step)}/{'rougeLsum'}": metrics['rougeLsum']})
            elif m == 'bertscore':
                wandb_logger.log_metrics(
                    {f"{data_split}_{str(self.global_step)}/{'bertscore_f1'}": metrics['bertscore_f1']})
            else:
                wandb_logger.log_metrics({f"{data_split}_{str(self.global_step)}/{m}": metrics[m]})
        self.log_text_container = pd.DataFrame({
            'source_texts': source_texts,
            'generated_texts': predictions,
            'target_texts': list(chain(*target_texts))
        })

    def on_validation_end(self):
        # log texts
        data_split = 'val'
        if self.log_text and self.global_step % LOG_TEXT_INTERVAL == 0:
            wandb_logger.log_text(
                key=f"{data_split}_{self.global_step}",
                dataframe=self.log_text_container.loc[:NUM_LOG_TEXT_ROWS - 1, :])

    def test_step(self, batch, batch_size):
        input_ids = batch['text_input_ids']
        attention_mask = batch['text_attention_mask']
        labels = batch['labels']

        loss, outputs = self(
            input_ids=input_ids,
            attention_mask=attention_mask,
            labels=labels
        )
        # the output of self() is: odict_keys(['loss', 'logits', 'past_key_values', 'encoder_last_hidden_state'])
        self.log("test_loss", loss, batch_size=batch_size, sync_dist=True)
        if self.log_text:
            self._generate_step(batch, 'test')

        return loss

    def configure_optimizers(self):
        return torch.optim.AdamW(self.parameters(), lr=LR)

    def calculate_common_metrics(self,
                                 source_texts: list,
                                 predictions: list,
                                 target_texts: list):
        """
        Computes common metrics.
        Args:
            source_texts: list of str, source texts
            predictions: list of str, generated texts
            target_texts: list of list, by convention found in the datasets library

        Returns:
            dict, keyed by `metrics`
        """
        metrics_dict = {}
        predictions = [p.strip() for p in predictions]
        target_texts = [[t_.strip() for t_ in t] for t in target_texts]
        source_texts = [r.strip() for r in source_texts]
        for metric in self.metrics:
            if metric == 'sacrebleu':
                metrics_dict.update({'sacrebleu':
                    np.mean(
                        self.metric_sacrebleu.compute(predictions=predictions, references=target_texts)[
                            'score'])})
            elif metric == 'meteor':
                metrics_dict.update({'meteor': np.mean(
                    self.metric_meteor.compute(predictions=predictions, references=target_texts)['meteor'])})
            elif metric == 'bertscore':
                _bertscore = self.metric_bertscore.compute(predictions=predictions, references=target_texts, lang='en')
                metrics_dict.update({'bertscore_f1': np.mean(_bertscore['f1'])})
                # metrics_dict.update({'bertscore_precision': np.mean(_bertscore['precision'])})
                # metrics_dict.update({'bertscore_recall': np.mean(_bertscore['recall'])})
            elif metric == 'sari':
                metrics_dict.update(
                    {'sari': np.mean(self.metric_sari.compute(sources=source_texts,
                                                              predictions=predictions,
                                                              references=target_texts)['sari'])})
            elif metric == 'rouge':
                _rouge = self.metric_rouge.compute(predictions=predictions, references=target_texts)
                metrics_dict.update({'rouge1': np.mean(_rouge['rouge1'])})
                metrics_dict.update({'rouge2': np.mean(_rouge['rouge2'])})
                metrics_dict.update({'rougeL': np.mean(_rouge['rougeL'])})
                metrics_dict.update({'rougeLsum': np.mean(_rouge['rougeLsum'])})
            elif metric == 'bleu':
                metrics_dict.update({'bleu': self.metric_bleu.compute(predictions=predictions,
                                                                      references=target_texts)['bleu']})

        return metrics_dict


if __name__ == '__main__':

    pl.seed_everything(0)

    parser = argparse.ArgumentParser(
        description="Producing results in Scientific Abstract Simplification Experiments"
    )
    parser.add_argument(
        "-t",
        "--task",
        default="sas_baseline",
        choices=TASKS,
        help="'sas' tasks are main experiments; 'ablation' tasks are for ablation analyses; the rest is testing how "
             "many steps before a single-instruction task would converge. ",
    )
    parser.add_argument(
        "-v",
        "--version",
        action="version",
        version="%(prog)s (version {version})".format(version=__version__),
    )
    args = parser.parse_args()

    # init logger
    wandb_logger = WandbLogger(name=RUN_NAME,
                               project=PROJECT_NAME,
                               save_dir='wandb')

    # prepare the data module
    sas_count, contextualization_count, summarization_count, simplification_count = 0, 0, 0, 0

    dm = SASDataModule(task=TASK,
                       tokenizer=BASE_MODEL_NAME,
                       batch_size=BATCH_SIZE,
                       max_source_length=MAX_SOURCE_LENGTH, max_target_length=MAX_TARGET_LENGTH,
                       num_workers=NUM_WORKERS)

    # prepare the model
    if RESUME_FROM_CKPT_PATH:
        model = SASModel.load_from_checkpoint(checkpoint_path=RESUME_FROM_CKPT_PATH)
    else:
        model = SASModel(model_name=BASE_MODEL_NAME,
                         log_text=LOG_TEXT,
                         metrics=METRICS,
                         fp16=False)

    # prepare the trainer
    trainer = pl.Trainer(
        # fast dev
        # fast_dev_run=500,
        # distributed training
        accelerator='gpu',
        devices=2,
        strategy='FSDP',
        # max_epochs=200,
        max_steps=MTL_PRETRAINING_TOTAL_STEPS,
        log_every_n_steps=500,
        val_check_interval=10000,
        callbacks=[ModelCheckpoint(
            dirpath=CKPTS_DIR,
            every_n_train_steps=MTL_PRETRAINING_TOTAL_STEPS,
            # every_n_train_steps=1000,
            filename='{epoch}-{step}-{val_loss:.3f}',
            save_top_k=3,
            verbose=True,
            monitor='val_loss',
            mode='min',
            save_on_train_epoch_end=True),
            LearningRateMonitor("step"),
            # EarlyStopping(monitor="val_loss", min_delta=0, patience=PATIENCE, verbose=False,
            #               mode="min"),
            TQDMProgressBar(refresh_rate=1)],
        # misc
        # deterministic=True,
        enable_progress_bar=True,
        logger=wandb_logger,
        default_root_dir=CKPTS_DIR,
    )

    trainer.fit(model=model, datamodule=dm)
