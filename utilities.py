#!/usr/bin/env python
# coding=utf-8

__author__ = "hw56@indiana.edu"
# __version__ = "alpha"
__license__ = "ISC"

import json
import evaluate
import numpy as np
from torch.utils.data import Dataset

def list2jsonl(data_dict_list, path):
    with open(path, 'w') as f:
        for line in data_dict_list:
            json.dump(line, f)
            f.write('\n')


def jsonl2list(path):
    data_dict_list = []
    with open(path, 'r') as f:
        data_dict_list_raw = list(f)
    for elem in data_dict_list_raw:
        data_dict_list.append(json.loads(elem))
    return data_dict_list


metric_sacrebleu = evaluate.load('sacrebleu')
metric_meteor = evaluate.load('meteor')
metric_bertscore = evaluate.load('bertscore', device='cpu')
metric_sari = evaluate.load('sari')
metric_rouge = evaluate.load('rouge')
metric_bleu = evaluate.load('bleu')


def calculate_common_metrics(metrics,
                             source_texts: list,
                             predictions: list,
                             target_texts: list):
    """
    Computes common metrics.
    Args:
        source_texts: list of str, source texts
        predictions: list of str, generated texts
        target_texts: list of list, by convention found in the datasets library

    Returns:
        dict, keyed by `metrics`
    """
    metrics_dict = {}
    predictions = [p.strip() for p in predictions]
    target_texts = [[t.strip()] for t in target_texts]
    source_texts = [r.strip() for r in source_texts]
    for metric in metrics:
        if metric == 'sacrebleu':
            metrics_dict.update({'sacrebleu':
                np.mean(
                    metric_sacrebleu.compute(predictions=predictions, references=target_texts)[
                        'score'])})
        elif metric == 'meteor':
            metrics_dict.update({'meteor': np.mean(
                metric_meteor.compute(predictions=predictions, references=target_texts)['meteor'])})
        elif metric == 'bertscore':
            _bertscore = metric_bertscore.compute(predictions=predictions, references=target_texts, lang='en', device='cpu')
            metrics_dict.update({'bertscore_f1': np.mean(_bertscore['f1'])})
            # metrics_dict.update({'bertscore_precision': np.mean(_bertscore['precision'])})
            # metrics_dict.update({'bertscore_recall': np.mean(_bertscore['recall'])})
        elif metric == 'sari':
            metrics_dict.update(
                {'sari': np.mean(metric_sari.compute(sources=source_texts,
                                                     predictions=predictions,
                                                     references=target_texts)['sari'])})
        elif metric == 'rouge':
            _rouge = metric_rouge.compute(predictions=predictions, references=target_texts)
            metrics_dict.update({'rouge1': np.mean(_rouge['rouge1'])})
            metrics_dict.update({'rouge2': np.mean(_rouge['rouge2'])})
            metrics_dict.update({'rougeL': np.mean(_rouge['rougeL'])})
            metrics_dict.update({'rougeLsum': np.mean(_rouge['rougeLsum'])})
        elif metric == 'bleu':
            metrics_dict.update({'bleu': metric_bleu.compute(predictions=predictions,
                                                             references=target_texts)['bleu']})

    return metrics_dict


def prepare_datasets_split_for_concat(dataset: Dataset,
                                      data_split: str,
                                      task_name: str):
    new = dataset[data_split]
    new = new.flatten()
    new = new.rename_column("translation.source", 'source')
    new = new.rename_column("translation.target", 'target')
    new = new.remove_columns([f for f in new.features if f.startswith('translation')])
    new = new.add_column(name="task", column=[task_name] * new.num_rows)
    return new
