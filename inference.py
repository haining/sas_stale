# coding=utf-8

import pickle
import datasets
import utilities as ut
from constants import *
from transformers import T5TokenizerFast
from train import SASModel


def transform(frozen_pl_model,
              instruction: str,
              source_text: str,
              top_p: float = TOP_P_PROB,
              repetition_penalty: float = REPETITION_PENALTY,
              do_sample: bool = True,
              max_source_length: str = MAX_SOURCE_LENGTH,
              max_target_length: str = MAX_TARGET_LENGTH):
    """
    """
    encoding = tokenizer(
        instruction + source_text,
        max_length=max_source_length,
        padding='max_length',
        truncation=True,
        return_tensors='pt').to(device)

    decoded_ids = frozen_pl_model.model.generate(
        input_ids=encoding['input_ids'],
        attention_mask=encoding['attention_mask'],
        max_new_tokens=max_target_length,
        top_p=top_p,
        repetition_penalty=repetition_penalty,
        do_sample=do_sample
    ).to(device)
    generated_text = [t.strip() for t in tokenizer.batch_decode(decoded_ids, skip_special_tokens=True)][0]

    return generated_text


if __name__ == "__main__":
    device = "cpu"
    tokenizer = T5TokenizerFast.from_pretrained(BASE_MODEL_NAME)

    inference_model = SASModel.load_from_checkpoint(CKPT_SAS_BASELINE).to(device)
    inference_model.freeze()

    # read in test corpus
    corpus = datasets.load_from_disk(SAS_DATASET_PATH)
    source_texts = [d['source'] for d in corpus['test']['translation']]

    generated_texts = [transform(inference_model,
                                 instruction=TASK2INSTRUCTION['sas_baseline'],
                                 source_text=t,
                                 top_p=.90,
                                 repetition_penalty=1.0,
                                 do_sample=True) for t in source_texts]
    # save
    pickle.dump(generated_texts, open('results/sas_baseline/topp.9_repenal1.0_text.pkl', 'wb'))

    # evaluate
    from importlib import reload
    reload(ut)
    metrics_dict = ut.calculate_common_metrics(['sacrebleu', 'meteor', 'sari', 'rouge', 'bertscore'],
                                               source_texts=source_texts,
                                               predictions=generated_texts,
                                               target_texts=[d['target'] for d in corpus['test']['translation']])
    # metrics_dict = calculate_common_metrics(['sacrebleu', 'meteor', 'sari', 'rouge', 'bertscore'],
    #                                            source_texts=source_texts,
    #                                            predictions=generated_texts,
    #                                            target_texts=[d['target'] for d in corpus['test']['translation']])


