# BASE_MODEL_NAME = 'google/t5-efficient-base-nl24'
BASE_MODEL_NAME = 'google/flan-t5-large'
# MAX_SOURCE_LENGTH = 512
MAX_SOURCE_LENGTH = 512 + 128 + 32  # 672
MAX_TARGET_LENGTH = 512
NUM_WORKERS = 8
VALIDATION_CAP = 1000
TASKS = ['sas_baseline', 'sas_full', 'sas_pretraining',
         "ablation_no_contextualization", "ablation_no_simplification", "alation_no_summarization",
         "contextualization", "simplification", "summarization"]
TASK2INSTRUCTION = {'sas_baseline': 'summarize, simplify, and contextualize: ',  # todo
                    'sas_full': 'summarize, simplify, and contextualize: ',  # todo
                    "ablation_no_contextualization": "summarize and simplify: ",
                    "ablation_no_simplification": "contextualize and summarize: ",
                    "alation_no_summarization": "contextualize and simplify: ",
                    "contextualization": "contextualize: ",
                    "simplification": "simplify: ",
                    "summarization": "summarize: "}
# TASK_INDEX2TASK = {0: 'sas_baseline', 1: 'contextualization', 2: 'summarization', 3: 'simplification'}
TASK_LENGTH_DIST_TRAIN = {'sas_baseline': 3030, 'contextualization': 732,
                          'summarization': 287113, 'simplification': 28364}
TASK_LENGTH_DIST_VAL = {'sas_baseline': 200, 'contextualization': 91,
                        'summarization': VALIDATION_CAP, 'simplification': 1000}
TASK_LENGTH_DIST_TEST = {'sas_baseline': 200, 'contextualization': 92,
                         'summarization': 11490, 'simplification': 1000}

# WANDB
PROJECT_NAME = "sas"
# TASK = 'sas_baseline'
# TASK = 'contextualization'
# TASK = 'simplification'
# TASK = 'summarization'
# TASK = 'sas_full'
TASK = 'sas_pretraining'
RUN_NAME = f'{TASK}_{BASE_MODEL_NAME.split("/")[1]}' + ''
CKPTS_DIR = f"ckpts_{RUN_NAME}"

# whether to resume training with a ckpt
RESUME_FROM_CKPT_PATH = ''
# four instructions
# RESUME_FROM_CKPT_PATH = 'ckpts_sas_full_flan-t5-large/epoch=1-step=263400-val_loss=1.503.ckpt'
# three instructions
# RESUME_FROM_CKPT_PATH = 'ckpts_sas_pretraining_flan-t5-large/epoch=1-step=224200-val_loss=1.467.ckpt'

# to log generated texts
PATIENCE = 20  # checks before early stopping
BATCH_SIZE = 1
NUM_LOG_TEXT_ROWS = 100
LOG_TEXT_INTERVAL = 1e4
TOP_P_PROB = .90
REPETITION_PENALTY = 1.2
LOG_TEXT = False
ALL_METRICS = ['sacrebleu', 'meteor', 'bertscore', 'sari', 'rouge', 'bleu']
METRICS = ['sacrebleu', 'meteor', 'sari', 'rouge', 'bleu']
# BIBLE_NUM_VAL_SAMPLES = 500
LR = 3e-5
# LR_CONT = 1e-5

# datasets
SAS_DATASET_PATH = 'resource/sas_dataset_hf'
SC_DATASET_ABS2CONT_PATH = 'resource/scientific_contextualization_dataset_hf_abs2context'
SC_DATASET_ABS2CONT_ABS_PATH = 'resource/scientific_contextualization_dataset_hf_abs2context_abs'
WIKIAUTO_DATASET_PATH = 'resource/wikiauto_acl_local'
CNN_DAILYMAIL_DATASET_PATH = 'resource/cnn_dailymail_2_local'

# checkpoints
CKPT_SC_ABS2CONT = 'ckpts_contextualization_flan-t5-large/epoch=6-step=2200-val_loss=2.336.ckpt'
# CKPT_SC_ABS2CONT_ABS = 'ckpts_contextualization_flan-t5-large/epoch=3-step=1200-val_loss=0.467.ckpt'
CKPT_SAS_BASELINE = 'ckpts_sas_baseline_flan-t5-large/epoch=25-step=39200-val_loss=1.538.ckpt'
CKPT_SIMPLIFICATION = 'ckpts_simplification_flan-t5-large/epoch=4-step=57000-val_loss=0.835.ckpt'
CKPT_SUMMARIZATION = 'ckpts_summarization_flan-t5-large/epoch=1-step=165000-val_loss=1.361.ckpt'
CKPT_DEMO = CKPT_SAS_BASELINE

SC_ABS2CONT_STEPS = int(CKPT_SC_ABS2CONT.split('=')[2].split('-')[0])
# SC_ABS2CONT_ABS_STEPS = int(CKPT_SC_ABS2CONT_ABS.split('=')[2].split('-')[0])
SUMMARIZATION_STEPS = int(CKPT_SUMMARIZATION.split('=')[2].split('-')[0])
SIMPLIFICATION_STEPS = int(CKPT_SIMPLIFICATION.split('=')[2].split('-')[0])
SAS_BASELINE_STEPS = int(CKPT_SAS_BASELINE.split('=')[2].split('-')[0])

# CORPUS_NAMES_SAS_FULL = ['sas_baseline', 'contextualization', 'summarization', 'simplification']
# CORPUS_NAMES_SAS_PRETRAINING = ['contextualization', 'summarization', 'simplification']
# CORPUS_NAMES_MTL_PRETRAINING = ['contextualization', 'summarization', 'simplification']
MTL_TOTAL_STEPS = SAS_BASELINE_STEPS + SC_ABS2CONT_STEPS + SUMMARIZATION_STEPS + SIMPLIFICATION_STEPS
MTL_PRETRAINING_TOTAL_STEPS = SC_ABS2CONT_STEPS + SUMMARIZATION_STEPS + SIMPLIFICATION_STEPS

# result ckpts
CKPTS_BASELINE = 'ckpts_sas_baseline_flan-t5-large/epoch=25-step=39200-val_loss=1.538.ckpt'
CKPTS_FULL = 'ckpts_sas_baseline_flan-t5-large/epoch=13-step=20500-val_loss=1.477.ckpt'  # four tasks
# CKPTS_PRETRAINING = 'ckpts_sas_baseline_flan-t5-large/'   # three tasks
