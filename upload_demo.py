from constants import *
from train import SASModel
from transformers import T5TokenizerFast


device = "cpu"
# tokenizer = T5TokenizerFast.from_pretrained(BASE_MODEL_NAME)

inference_model = SASModel.load_from_checkpoint(CKPT_DEMO).to(device)
inference_model.model.push_to_hub('haining/sas_baseline')